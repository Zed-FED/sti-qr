# To start the project
run `npm run dev` in your command line

# To make changes in HTML
open the directory `src/view` in which you will find `.twig` file 

# Production Build
run `npm run build` in your command line for optimized build for production.

# For HTML
find `dist` folder for `static HTML`.
