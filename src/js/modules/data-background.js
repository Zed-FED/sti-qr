;(function($) {
   var $dataBackground = $("[data-background]");
   var $dataBackgroundPosition = $("[data-background-position]");

   if ($dataBackground.length > 0) {
     $dataBackground.each(function() {
       var bg = $(this).attr("data-background");
       if (bg.match("^rgb") || bg.match("^#")) {
         $(this).css("background-color", bg);
       } else {
         $(this).css("background-image", "url(" + bg + ")");
       }
     });

     $dataBackgroundPosition.each(function() {
       var bgPosition = $(this).attr("data-background-position");
       $(this).css("background-position", bgPosition);
     });
   }
 })(jQuery);
