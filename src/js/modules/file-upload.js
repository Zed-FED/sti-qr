import $ from 'jquery';

$("form").on("change", ".file-upload-field", function(){ 
    $(this).parent(".file-upload-wrapper").attr("data-text", $(this).val().replace(/.*(\/|\\)/, '') );
        
    var attrLength = $(".file-upload-wrapper").attr("data-text").length;
    if(attrLength > 0) {
     $(".file-upload-wrapper").addClass('file-selected');
    } else {
        $(".file-upload-wrapper").removeClass('file-selected');
    }
});