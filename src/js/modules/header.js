import $ from 'jquery';

$(window).scroll(function() {
    var scroll = $(window).scrollTop();

    if( scroll >= 100 ) {
        $('.site-header').addClass('sticky');
    } else {
        $('.site-header').removeClass('sticky');
    }

    if($('.site-header').hasClass('sticky')) {
        $('.hero-logo').css({'opacity':'0', 'transition': '0.5s ease-in-out'});
    } else {
        $('.hero-logo').css('opacity', '1');
    }
})

$('.nav-button').click(function(){
    $(this).toggleClass('active');
    $('.site-nav').toggleClass('active');
})