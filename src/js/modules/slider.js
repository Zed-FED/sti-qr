import $ from 'jquery';
import 'slick-carousel/slick/slick.min.js';

$(document).ready(function(){
  $('.slider-wrapper').slick({
    dots: true,
    arrows: false
  });
});
